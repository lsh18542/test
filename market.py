# -*- coding: utf-8 -*-
import json
import re
import requests
import urllib.request

from bs4 import BeautifulSoup
from flask import Flask, request
from slack import WebClient
from slack.web.classes.blocks import *
from slack.web.classes.elements import *
from slack.web.classes.messages import Message
from slack.web.classes.interactions import MessageInteractiveEvent
from slackeventsapi import SlackEventAdapter
import thread

SLACK_TOKEN = "xoxb-678362642579-689765018400-SmgdVbNg3iugxClU5SFB8yQ9"
SLACK_SIGNING_SECRET = "2d76eeeaa216a547f32c0368b8424cce"


app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)


# 키워드로 중고거래 사이트를 크롤링하여 가격대가 비슷한 상품을 찾은 다음,
# 메시지를 만들어주는 함수입니다
def make_sale_message(keyword, price):
    print(f'{keyword!r}, {price!r}로 검색합니다...')
    # 주어진 키워드로 중고거래 사이트를 크롤링해 보세요

    query = keyword.encode('unicode_escape').decode('utf-8').replace("\\", "%")
    print('쿼리 :', query)
    url = "http://corners.auction.co.kr/corner/UsedMarketList.aspx?keyword=" + query
    sourcecode = urllib.request.urlopen(url).read()
    
    soup = BeautifulSoup(sourcecode, 'html.parser')
    title_codes = soup.find_all('div', class_='item_title type1')
    # print(title_codes)
    img_codes = soup.find_all('div', class_='image_info')
    price_codes = soup.find_all('span', class_='now')
    print("결과 수 : ", len(title_codes))
    
    items = []
    for item_idx in range(len(title_codes)):
        if item_idx > 5:
            item = {}
            item["title"] = title_codes[item_idx].get_text().strip()
            item["link"] = title_codes[item_idx].find('a')['href'].strip()
            item["image"] = img_codes[item_idx].find('img')['src'].strip()
            item_page = BeautifulSoup(urllib.request.urlopen(item['link'].replace(keyword, query)).read(), 'html.parser')
            # item["date"] = 
            item["buy_type"] = item_page.find('ul', class_='prodnoti_lst').find_all('span', class_='cont')[1].get_text().strip()
            item["price"] = price_codes[item_idx].get_text().strip()
            
            items.append(item)

    # 메시지를 꾸밉니다
    # 처음 섹션에는 제목과 첫 번째 상품의 사진을 넣습니다
    head_section = SectionBlock(
        text="*\"" + keyword + "\", " + str(price) + "원으로 검색한 결과입니다.*",
        accessory=ImageElement(image_url=items[0]["image"], alt_text=keyword)
    )

    # 두 번째 섹션에는 처음 4개의 상품을 제목 링크/내용으로 넣습니다
    # (한 섹션에는 텍스트를 최대 10개까지 넣을 수 있습니다)
    link_section = SectionBlock(
        fields=[
            "*<" + items[0]["link"] + "|" + items[0]["title"] + ">*",
            "*<" + items[1]["link"] + "|" + items[1]["title"] + ">*",
            items[0]["buy_type"] + " / " + items[0]["price"],
            items[1]["buy_type"] + " / " + items[1]["price"],
            "*<" + items[2]["link"] + "|" + items[2]["title"] + ">*",
            "*<" + items[3]["link"] + "|" + items[3]["title"] + ">*",
            items[2]["buy_type"] + " / " + items[2]["price"],
            items[3]["buy_type"] + " / " + items[3]["price"],
        ]
    )

    # 마지막 섹션에는 가격대를 바꾸는 버튼을 추가합니다
    # 여러 개의 버튼을 넣을 땐 ActionsBlock을 사용합니다 (5개까지 가능)
    button_actions = ActionsBlock(
        elements=[
            ButtonElement(
                text="1만원 올리기",
                action_id="price_up_1", value=str(price + 10000)
            ),
            ButtonElement(
                text="5만원 올리기", style="danger",
                action_id="price_up_5", value=str(price + 50000)
            ),
            ButtonElement(
                text="1만원 낮추기",
                action_id="price_down_1", value=str(price - 10000)
            ),
            ButtonElement(
                text="5만원 낮추기", style="primary",
                action_id="price_down_5", value=str(price - 50000)
            ),
        ]
    )

    # 각 섹션을 하나의 메시지로 묶습니다
    message = Message(
        text=keyword,   # 키워드를 메시지의 텍스트로 저장합니다 (보이지 않음)
        blocks=[head_section, link_section, button_actions]
    )

    return message


# 챗봇이 멘션을 받으면 중고거래 사이트를 검색합니다
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]

    # 입력한 텍스트에서 검색 키워드와 가격대를 뽑아냅니다.
    keyword = text.split()[1].strip()
    price = int(text.split()[2].strip()[:-1])

    # 중고거래 사이트를 크롤링하여 가격대가 비슷한 상품을 찾아옵니다.
    message = make_sale_message(keyword, price)
    # 메시지를 채널에 올립니다
    slack_web_client.chat_postMessage(
        channel=channel,
        **message.to_dict(),
    )


# 사용자가 버튼을 클릭한 결과는 /click 으로 받습니다
@app.route("/click", methods=["GET", "POST"])
def on_button_click():
    # 버튼 클릭은 SlackEventsApi에서 처리해주지 않으므로 직접 처리합니다
    payload = request.values["payload"]
    event_data = MessageInteractiveEvent(json.loads(payload))

    keyword = text.split()[1]
    price = int(re.search('\d+', text.split()[2].group(0)))
    
    # 다른 가격대로 다시 크롤링합니다.
    message = make_sale_message(keyword, new_price)

    # 결과를 직접 response_url로 보내면 기존 메시지의 내용을 바꿀 수 있습니다
    requests.post(event_data.response_url, data=json.dumps(message.to_dict()))
    return "OK", 200


# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('127.0.0.1', port=5000)
