# -*- coding: utf-8 -*-
import re
import urllib.request

from bs4 import BeautifulSoup

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter
from slack.web.classes import extract_json
from slack.web.classes.blocks import *

SLACK_TOKEN = "xoxb-678362642579-689765018400-SmgdVbNg3iugxClU5SFB8yQ9"
SLACK_SIGNING_SECRET = "2d76eeeaa216a547f32c0368b8424cce"

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)


# 크롤링 함수 구현하기
def _crawl_music_chart(text):
    if not "music" in text:
        return "`@<봇이름> music` 과 같이 멘션해주세요."

    # 여기에 함수를 구현해봅시다.
    url = "https://music.bugs.co.kr/chart"
    sourcecode = urllib.request.urlopen(url).read()

    soup = BeautifulSoup(sourcecode, 'html.parser')

    data, song, artist = [], [], []
    top_num = 10
    for i in soup.find_all('p', class_="title"):
        if len(song) >= top_num:
            break
        else:
            song.append(i.get_text().strip())

    for i in soup.find_all('p', class_="artist"):
        if len(artist) >= top_num:
            break
        else:
            artist.append(i.find('a').get_text().strip())

    for i in range(len(song)):
        data.append(str(i + 1) +"위 : " + song[i] + ' / ' + artist[i])

    data.append("출처 : <https://music.bugs.co.kr/chart|벅스차트>")
    message = '\n'.join(data)
    return message


# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]

    block1 = SectionBlock(
        text = '테스트 테스트'
    )

    block2 = SectionBlock(
        fields = ['test txt1', 'test txt2', 'test txt3']
    )

    if 'music' in text:
        message = _crawl_music_chart(text)
        slack_web_client.chat_postMessage(
            channel=channel,
            text=message
        )
    elif 'test' in text:
        my_blocks = [block1, block2]
        slack_web_client.chat_postMessage(
            channel=channel,
            blocks=extract_json(my_blocks)
        )
    elif 'get_users' in text:
        response = slack_web_client.users_list()
        members = response['members']
        users = '\n'.join([response['members'][i]['name'] for i in range(len(response['members']))])
        slack_web_client.chat_postMessage(
            channel=channel,
            text="*Users list in this channel*\n" + users
        )  
    elif 'help' in text:
        slack_web_client.chat_postMessage(
            channel = channel,
            text="`@챗봇이름 music`, `@챗봇이름 test`, `@챗봇이름 get_users`를 입력하세요."
        )
    else:
        slack_web_client.chat_postMessage(
            channel=channel,
            text="`@챗봇이름 help`로 명령어를 알아보세요."
        )


# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)
