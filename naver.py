# -*- coding: utf-8 -*-
import re
import urllib.request

from bs4 import BeautifulSoup

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter
from threading import Thread

SLACK_TOKEN = "xoxb-678362642579-689765018400-SmgdVbNg3iugxClU5SFB8yQ9"

SLACK_SIGNING_SECRET = "2d76eeeaa216a547f32c0368b8424cce"


app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)


# 크롤링 함수 구현하기
def _crawl_naver_keywords(text):
    # 여기에 함수를 구현해봅시다.
    
    url = re.search(r'(https?://\S+)', text.split('|')[0]).group(0)
    req = urllib.request.Request(url)
    
    sourcecode = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(sourcecode, 'html.parser')
    
    keywords = []
    # enumerate(['a', 'b', 'c', 'd']) 
    # [(0, 'a'), (1, 'b'), (2, 'c'), (3, 'd')]
    for i, keyword in enumerate(soup.find_all("span", class_="ah_k")):
        if i < 20:
            keywords.append('%d위 : '% (i+1) +keyword.get_text())

    # 키워드 리스트를 문자열로 만듭니다.
    return '\n'.join(keywords)

# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]

    print("start post_message_thread")
    keywords = _crawl_naver_keywords(text)

    post_message_thread = Thread(target = post_message, args=(channel, keywords))
    post_message_thread.start()
    print("start post_message_thread")

def post_message(channel, keywords):
    slack_web_client.chat_postMessage(
        channel=channel,
        text=keywords
    )

# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)
