# -*- coding: utf-8 -*-
import re
import urllib.request

from bs4 import BeautifulSoup

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter

from slack.web.classes import extract_json
from slack.web.classes.blocks import *

SLACK_TOKEN = "xoxb-678362642579-689765018400-SmgdVbNg3iugxClU5SFB8yQ9"

SLACK_SIGNING_SECRET = "2d76eeeaa216a547f32c0368b8424cce"


app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    print(event_data)
    blocks = [ImageBlock(
            image_url="https://t1.daumcdn.net/liveboard/ziksir/2c8f661ed0ec459889b512bb1a4926cc.jpg",
            alt_text="image image"
        ), ImageBlock(
            image_url="http://placekitten.com/500/500",
            alt_text="image image"
        )]
    slack_web_client.chat_postMessage(
        channel=channel,
        blocks=extract_json(blocks)
    )


# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)
